#use wml::debian::template title="Las personas: quiénes somos, qué hacemos"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="ac8343f61b83fe988dbcb035d77af5bf09ba0709"

# translators: some text is taken from /intro/about.wml

<h2>Desarrolladores y contribuidores</h2>
<p>Debian es producido por cerca de un millar de desarrolladores
activos distribuidos
<a href="$(DEVEL)/developers.loc">por todo el mundo</a> que trabajan de forma voluntaria
en su tiempo libre.
Pocos de ellos se conocen en persona.
La comunicación se realiza, principalmente, a través de correo electrónico (listas de correo en
lists.debian.org) y de IRC (canal #debian en irc.debian.org).
</p>

<p>La lista completa de miembros oficiales de Debian se encuentra en
<a href="https://nm.debian.org/members">nm.debian.org</a>, donde se gestiona la
membresía. Una lista más amplia de contribuidores puede encontrarse en
<a href="https://contributors.debian.org">contributors.debian.org</a>.</p>

<p>El proyecto Debian tiene una <a href="organization">estructura organizada</a>
cuidadosamente. Si desea más información sobre cómo es Debian por dentro,
siéntase libre de navegar por el <a href="$(DEVEL)/">rincón del desarrollador</a>.</p>

<h3><a name="history">¿Cómo empezó todo?</a></h3>

<p>Debian comenzó en agosto de 1993 gracias a Ian Murdock como una nueva distribución
que se realizaría de forma abierta, en la línea del espíritu de Linux y GNU. Debian estaba pensada
para ser creada de forma cuidadosa y concienzuda, y ser mantenida y
soportada con el mismo cuidado. Comenzó como un grupo de pocos y fuertemente unidos
hackers de software libre y creció gradualmente hasta convertirse en una comunidad grande y bien
organizada de desarrolladores y usuarios. Vea
<a href="$(DOC)/manuals/project-history/">la historia detallada</a>.</p>

<p>Ya que mucha gente lo ha preguntado, Debian se pronuncia /&#712;de.bi.&#601;n/.
Viene de los nombres del creador de Debian, Ian Murdock, y de su esposa,
Debra.</p>
  
<h2>Individuos y organizaciones que apoyan a Debian</h2>

<p>Muchos otros individuos y organizaciones forman parte de la comunidad Debian:
<ul>
  <li><a href="https://db.debian.org/machines.cgi">Patrocinadores de alojamiento de servicios («hosting») y de hardware</a></li>
  <li><a href="../mirror/sponsors">Patrocinadores de sitios de réplica</a></li>
  <li><a href="../partners/">Socios de desarrollo y de servicios</a></li>
  <li><a href="../consultants">Consultores</a></li>
  <li><a href="../CD/vendors">Vendedores de medios de instalación de Debian</a></li>
  <li><a href="../distrib/pre-installed">Vendedores de ordenadores que preinstalan Debian</a></li>
  <li><a href="../events/merchandise">Vendedores de material promocional</a></li>
</ul>

<h2><a name="users">¿Quién usa Debian?</a></h2>

<p>Aunque no hay disponibles estadísticas precisas (ya que Debian no
requiere que los usuarios se registren), hay signos bastante evidentes de que Debian es
usado por una amplia variedad de organizaciones, grandes y pequeñas, así como por
muchos miles de personas de forma individual. Puede ver en nuestra página <a href="../users/">¿Quién
usa Debian?</a> una lista de organizaciones de importancia que
han enviado una breve reseña sobre cómo y por qué utilizan Debian.</p>
