#use wml::debian::template title="Debian 10 -- Errata" BARETITLE=true
#use wml::debian::toc

#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="92c99e5d868735bb9bb0084a44c322cc710a97c3"

<toc-display/>


# <toc-add-entry name="known_probs">Bekannte Probleme</toc-add-entry>

<toc-add-entry name="security">Sicherheitsprobleme</toc-add-entry>

<p>Das Debian-Sicherheitsteam stellt Aktualisierungen von Paketen in der
stabilen Veröffentlichung bereit, in denen Sicherheitsprobleme erkannt wurden.
Bitte lesen Sie die <a href="$(HOME)/security/">Sicherheitsseiten</a>
bezüglich weiterer Informationen über alle Sicherheitsprobleme, die in
<q>Buster</q> erkannt wurden.</p>

<p>Wenn Sie APT verwenden, fügen Sie die folgende Zeile zu
<tt>/etc/apt/sources.list</tt> hinzu, um auf die neuesten
Sicherheitsaktualisierungen zugreifen zu können:

<pre>
  deb http://security.debian.org/ buster/updates main contrib non-free
</pre>

<p>Führen Sie danach <kbd>apt update</kbd> gefolgt von
<kbd>apt upgrade</kbd> aus.</p>


<toc-add-entry name="pointrelease">Zwischenveröffentlichungen</toc-add-entry>

<p>Manchmal wird die veröffentlichte Distribution aktualisiert, wenn mehrere
   kritische Probleme aufgetreten oder Sicherheitsaktualisierungen
   herausgebracht worden sind. Im
   Allgemeinen wird dies als Zwischenveröffentlichung bezeichnet.</p>

<ul>
  <li>Die erste Zwischenveröffentlichung (10.1) wurde am
      <a href="$(HOME)/News/2019/20190907">07. September 2019</a> veröffentlicht.</li>
  <li>Die zweite Zwischenveröffentlichung (10.2) wurde am
      <a href="$(HOME)/News/2019/20191116">16. November 2019</a> veröffentlicht.</li>
 <li>Die dritte Zwischenveröffentlichung (10.3) wurde am
      <a href="$(HOME)/News/2020/20200208">08. Februar 2020</a> veröffentlicht.</li>
 <li>Die vierte Zwischenveröffentlichung (10.4) wurde am
      <a href="$(HOME)/News/2020/20200509">09. Mai 2020</a> veröffentlicht.</li>
 <li>Die fünfte Zwischenveröffentlichung (10.5) wurde am
      <a href="$(HOME)/News/2020/20200801">01. August 2020</a> veröffentlicht.</li>
 <li>Die sechste Zwischenveröffentlichung (10.6) wurde am
      <a href="$(HOME)/News/2020/20200926">26. September 2020</a> veröffentlicht.</li>
 <li>Die siebte Zwischenveröffentlichung (10.7) wurde am
      <a href="$(HOME)/News/2020/20201205">05. Dezember 2020</a> veröffentlicht.</li>
 <li>Die achte Zwischenveröffentlichung (10.8) wurde am
      <a href="$(HOME)/News/2021/20210206">06. Februar 2021</a> veröffentlicht.</li> 

</ul>

<ifeq <current_release_buster> 10.0 "

<p>Es gibt derzeit noch keine Zwischenveröffentlichungen für Debian 10.</p>" "

<p>Details über die Änderungen zwischen 10.0 und <current_release_buster/>
finden Sie im <a
href=http://http.us.debian.org/debian/dists/buster/ChangeLog>\
Änderungsprotokoll (Changelog)</a>.</p>"/>

<p>Korrekturen für die veröffentlichte stabile Distribution durchlaufen oft
eine ausgedehnte Testperiode, bevor sie im Archiv akzeptiert werden. Diese
Korrekturen sind allerdings im Verzeichnis
<a href="http://ftp.debian.org/debian/dists/buster-proposed-updates/">\
dists/buster-proposed-updates</a> jedes Debian-Archiv-Spiegels verfügbar.</p>

<p>Falls Sie APT zur Aktualisierung Ihrer Pakete verwenden, können Sie diese
vorgeschlagenen Änderungen (proposed-updates) installieren, indem Sie folgende
Zeilen zu <tt>/etc/apt/sources.list</tt> hinzufügen:</p>

<pre>
  \# vorgeschlagene Änderungen für eine Debian 10 Zwischenveröffentlichung
  deb http://deb.debian.org/debian buster-proposed-updates main contrib non-free
</pre>

<p>Führen Sie danach <kbd>apt update</kbd> gefolgt von
<kbd>apt upgrade</kbd> aus.</p>


<toc-add-entry name="installer">Installationssystem</toc-add-entry>

<p>
Informationen über Errata und Aktualisierungen für das Installationssystem
finden Sie auf der <a href="debian-installer/">Webseite zum Debian-Installer</a>.
</p>
