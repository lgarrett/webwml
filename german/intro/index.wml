#use wml::debian::template title="Einführung in Debian" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="93f9b3008408920efa7a3791fb329de4c58aac6c"
# Translator: Martin Schulze <joey@infodrom.north.de>
# Updated: Holger Wansing <linux@wansing-online.de>, 2016.
# Updated: Holger Wansing <hwansing@mailbox.org>, 2020.

<a id=community></a>
<h2>Debian ist eine Gemeinschaft von Menschen</h2>
<p>Tausende von Freiwilligen auf der ganzen Welt arbeiten zusammen am Ziel Freie
   Software und zum Nutzen der Anwender.</p>

<ul>
  <li>
    <a href="people">Menschen:</a>
    Wer wir sind, was wir tun
  </li>
  <li>
    <a href="philosophy">Philosophie:</a>
    Warum und wie wir es tun
  </li>
  <li>
    <a href="../devel/join/">Mach mit, du kannst etwas beitragen:</a>
    Du kannst ein Teil von uns sein!
  </li>
  <li>
    <a href="help">Wie kannst du Debian helfen?</a>
  </li>
  <li>
    <a href="../social_contract">Gesellschaftsvertrag:</a>
    Unsere Moralvorstellungen
  </li>
  <li>
    <a href="diversity">Stellungnahme zur Vielfalt</a>
  </li>
  <li>
    <a href="../code_of_conduct">Verhaltenskodex</a>
  </li>
  <li>
    <a href="../partners/">Partner:</a>
    Firmen und Organisationen, die das Debian-Projekt fortlaufend unterstützen
  </li>
  <li>
    <a href="../donations">Spenden</a>
  </li>
  <li>
    <a href="../legal/">Rechtliche Themen</a>
  </li>
  <li>
    <a href="../legal/privacy">Datenschutz</a>
  </li>
  <li>
    <a href="../contact">Kontaktiere uns</a>
  </li>
</ul>

<hr>

<a id=software></a>
<h2>Debian ist ein freies Betriebssystem</h2>
<p>Beginnend mit Linux, fügen wir viele Tausend Anwendungen hinzu, um den Bedürfnissen unserer
   Anwender gerecht zu werden.</p>
<ul>
  <li>
    <a href="../distrib">Download:</a>
    Weitere Varianten der Debian-Images
  </li>
  <li>
  <a href="why_debian">Warum Debian</a>
  </li>
  <li>
    <a href="../support">Unterstützung:</a>
    Hilfe anfordern
  </li>
  <li>
    <a href="../security">Sicherheit:</a>
    Letztes Update<br>
    <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
        @MYLIST = split(/\n/, $MYLIST);
        $MYLIST[0] =~ s#security#../security#;
        print $MYLIST[0]; }:>
  </li>
  <li>
    <a href="../distrib/packages">Software-Pakete:</a>
    Stöbere in der reichhalten Liste unserer Anwendungen
  </li>
  <li>
    <a href="../doc">Dokumentation</a>
  </li>
  <li>
    <a href="https://wiki.debian.org">Debian Wiki</a>
  </li>
  <li>
    <a href="../Bugs">Fehlerberichte</a>
  </li>
  <li>
    <a href="https://lists.debian.org/">
    Mailinglisten</a>
  </li>
  <li>
    <a href="../blends">Pure Blends:</a>
    Metapakete für spezielle Ansprüche
  </li>
  <li>
    <a href="../devel">Entwicklerecke:</a>
    Informationen, die primär für Debian-Entwickler von Interesse sind
  </li>
  <li>
    <a href="../ports">Portierungen/Architekturen:</a>
    CPU-Architekturen, die wir unterstützen
  </li>
  <li>
    <a href="search">Informationen zur Verwendung von Debians Suchmaschine</a>
  </li>
  <li>
    <a href="cn">Debians Webseiten in verschiedenen Sprachen</a>
  </li>
</ul>
