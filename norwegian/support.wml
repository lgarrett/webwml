#use wml::debian::template title="Brukerstøtte"
#use wml::debian::toc
#use wml::debian::translation-check translation="6838e6aa35cea0dd360ea9a9f08965ebdc8c9d50" maintainer="Hans F. Nordhaug"
# Oversatt til norsk av Tor Slettnes <tor@slett.net>
# Oppdatert av Hans F. Nordhaug <hansfn@gmail.com>, 2008-2021

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

<p>Debian og dens brukerstøtte er drevet av et fellesskap av frivillige.</p>

<P>Hvis denne felleskap-drevne brukerstøtten ikke dekker denne behov, kan du
lese vår <a href="doc/">dokumentasjon</a> eller leie en
<a href="consultants/">konsulent</a>.

<toc-display />

<toc-add-entry name="irc">Sanntidshjelp via IRC</toc-add-entry>

<p><a href="http://www.irchelp.org/">IRC</a> (Internet Relay Chat) er en måte
å ha sanntidssamtaler mellom mennesker over hele verden. IRC-kanaler om Debian 
finnes på <a href="https://www.oftc.net/">OFTC</a>.</p>

<p>For å koble opp, trenger du et IRC klientprogram.  Noen av de mest
brukte klientene er 
<a href="https://packages.debian.org/stable/net/hexchat">HexChat</a>,
<a href="https://packages.debian.org/stable/net/ircii">ircII</a>,
<a href="https://packages.debian.org/stable/net/irssi">irssi</a>,
<a href="https://packages.debian.org/stable/net/epic5">epic5</a> og
<a href="https://packages.debian.org/stable/net/kvirc">KVIrc</a>,
som alle finnes som Debian pakker. 
OFTC tilbyr også et <a href="https://www.oftc.net/WebChat/">WebChat</a>
nettgrensesnitt som lar deg koble til IRC med en nettleser uten at du 
trenger å installere en lokal klient.</p>

<p>Når du har installert klienten må du be den om å koble opp til tjeneren.
I de fleste IRC-klientene kan du skrive:</p>

<pre>
/server irc.debian.org
</pre>

<p>I noen klienter (f.eks. irssi) så må du i stedet skrive :</p>

<pre>
/connect irc.debian.org
</pre>

# Note to translators:
# You might want to insert here a paragraph stating which IRC channel is available
# for user support in your language and pointing to the English IRC channel.
# <p>Once you are connected, join channel <code>#debian-foo</code> by typing</p>
# <pre>/join #debian</pre>
# for support in your language.
# <p>For support in English, read on</p>

<p>Etter at du har koblet opp, går du inn på kanalen <code>#debian</code>
ved å skrive:</p>

<pre>
/join #debian
</pre>

<p>Merk: HexChat og andre grafiske klienter er annerledes.  De bruker et
intuitivt grafisk brukergrensesnitt for å koble opp mot tjenere og å delta i
kanaler.</p>

<p>Etter dette finner du deg blant den vennlige flokken av
<code>#debian</code>-innbyggere. Still gjerne spørsmål (på engelsk) om Debian der.
Kanalens OSS er tilgjengelig på <url "https://wiki.debian.org/DebianIRC" />.</p>


<p>Det er flere andre IRC-nettverk hvor du også kan chatte om Debian. Et av
de mer populære er <a href="https://freenode.net/">IRC-nettverket freenode</a> 
på <kbd>chat.freenode.net</kbd>.</p>

<toc-add-entry name="mail_lists" href="MailingLists/">E-postlister</toc-add-entry>

<p>Debian er utviklet gjennom en distribuert innsats fra hele
verden. Derfor er e-post en foretrekket kanal for å diskutere forskjellige
emner. Mye av samtalen mellom Debians utviklere og brukere handteres gjennom
flere e-postlister.</p>
    
<p>Det fins mange offentlige tilgjengelige e-postlister. For mer informasjon,
se <a href="MailingLists/">Debians e-postlistesider</a>.</p>
    
# Note to translators:
# You might want to adapt the following paragraph, stating which list
# is available for user support in your language instead of English.

<p>For brukerstøtte på engelsk, kontakt e-postlisten 
<a href="https://lists.debian.org/debian-user/">debian-user</a>.
</p>

<p>For brukerstøtte på andre språk, sjekk indeksen over
<a href="https://lists.debian.org/users.html">e-postlister for brukere</a>.
</p>

<p>Det er selvfølgelig mange andre e-postlister, rettet mot spesifikke 
aspekter ved det omfattende Linux-økosystem, og som ikke er Debian-spesifikke.  
Bruk din foretrukne søkemotor til å finne den mest velegnede listen til ditt 
formål.</p>

<toc-add-entry name="usenet">Usenet nyhetsgrupper</toc-add-entry>

<p>Mange av våre <a href="#mail_lists">e-postlister</a> kan man lese som 
nyhetsgrupper, i hierarkiet <kbd>linux.debian.*</kbd>. Dette kan også gjøres med
et nettgrensesnitt som <a href="https://groups.google.com/forum/">Google Groups</a>.
</p>

<toc-add-entry name="web">Nettsteder</toc-add-entry>

<h3 id="forums">Forum</h3>

# Note to translators:
# If there is a specific Debian forum for your language you might want to
# insert here a paragraph stating which list is available for user support
# in your language and pointing to the English forums.
# <p><a href="http://someforum.example.org/">someforum</a> is a web portal
# on which you can use your language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>
#
# <p><a href="http://forums.debian.net">Debian User Forums</a> is a web portal
# on which you can use the English language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>

<p><a href="http://forums.debian.net">Debian-brukerforum</a> er et nettsted hvor 
du kan diskutere Debian-relaterte emner, stille spørsmål om Debian og få dem 
besvart av andre brukere.</p>

<toc-add-entry name="maintainers">Kontakte pakkevedlikeholdere</toc-add-entry>

<p>Det fins to måter å få tak i personer som vedlikeholder Debian-pakker. Dersom
du trenger å få tak i den ansvarlige på grunn av en programfeil, bare send inn
en feilrapport (se avsnittet om feilrapportering nedenfor).
Pakkevedlikeholderen vil motta en kopi av feilrapporten.</p>

<p>Dersom du bare vil ha en samtale med den pakkevedlikeholderen, kan du bruke et
spesielt e-post-alias som er satt opp for hver pakke.  All post sent til
&lt;<em>pakkenavn</em>&gt;@packages.debian.org vil bli videresendt til
personen som vedlikeholder den pakken.</p>

<p>Information on submitting bugs, viewing the currently active bugs, and
the bug tracking system in general can be found at the
<a href="Bugs/">bug tracking system web pages</a>.</p>

<toc-add-entry name="bts" href="Bugs/">Feilrapporteringssystemet</toc-add-entry>

<p>Debian-distribusjonen har et feilrapporteringssystem som
inneholder detaljinformasjon om programfeil innsendt av brukere og utviklere.
Hver enkelt feilrapport gis et nummer, og holdes på fil inntil den har blitt 
markert som behandlet.</p>

<p>For å rapportere en feil, kan du bruke nettsidene for feilrapportering 
nedenfor, eller du kan bruke Debian-programmet <q>reportbug</q> for å automatisere
innsendingen av en slik rapport.</p>

<p>Informasjon om å sende inn rapporter, se på feil under behandlig, og
feilrapporteringssystemet generelt fins på 
<a href="Bugs/">nettsidene til feilrapporteringssystemet</a>.</p>

<toc-add-entry name="consultants" href="consultants/">Konsulenter</toc-add-entry>

<p>Debian er fri programvare og tilbyr gratis hjelp via e-postlister. Noen folk
har enten ikke tid eller har spesielle behov, og er villig til å hyre noen til
enten å vedlikeholde eller legge til nye funksjonalitet til sitt
Debian-system. Se på <a href="consultants/">konsulentsidene</a> for en liste
over personer og bedrifter som tilbyr slike tjenester.</p>

<toc-add-entry name="release" href="releases/stable/">Kjente problemer</toc-add-entry>

<p>Begrensninger og alvorlige problemer med den aktuelle stabile
distribusjonen (om noen) er beskrevet på 
<a href="releases/stable/">utgivelsessidene</a>.</p>

<p>Vær særlig oppmerksom på 
<a href="releases/stable/releasenotes">utgivelsesmerknadene</a> og 
<a href="releases/stable/errata">listen med kjente feil</a> (errata).</p>
