#use wml::debian::cdimage title="최소 CD에서 네트워크 설치"
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="f917b9adf4a1c15cca8405e010043d380e4b1b83" maintainer="Sebul"

<p><q>네트워크 설치</q> 또는 <q>netinst</q> CD는 한 CD로 운영체제 전체를 설치할 수 있습니다.
이 CD는 설치 시작할 최소량의 소프트웨어를 포함하고 남은 패키지를 인터넷에서 가져옵니다.
</p>

<p><strong>무엇이 내게 더 좋은가 &mdash; 최소 부팅 가능 CD-ROM 또는 
전체 CD?</strong> 때에 따라 다르지만, 많은 경우 최소 CD가 좋습니다.
&mdash; 무엇보다, 여러분 컴퓨터에 설치할 것만 선택해서 다운로드하며, 그것은 시간과 대역을 절약합니다.
한편, 1개보다 많은 기계에 설치하거나 인터넷 연결이 자유롭지 않을 때는 전체 CD가 좋습니다.
</p>

<p><strong>설치 동안 어떤 타입의 네트워크 연결이 지원되나요?</strong>
네트워크 설치는 인터넷에 연결된 것을 가정합니다.
이를 위해 다양한 방법이 제공되는데, PPP 다이얼-업, 이더넷, WLAN(일부 제한 있음) 같은 것이며,
다만 ISDN은 아니다 &mdash; 죄송!</p>

<p>아래의 최소 부팅가능 CD 이미지를 다운로드 가능:</p>

<ul>

  <li>공식 <q>netinst</q> 이미지 <q>stable</q> 릴리스 용 &mdash; <a
  href="#netinst-stable">아래를 보세요</a></li>

  <li><q>testing</q> 릴리스를 위한 이미지, 매일 빌드한 버전 및 동작한다고 알려진 스냅샷,
<a href="$(DEVEL)/debian-installer/">Debian-Installer 페이지</a>를 보세요.</li>

</ul>


<h2 id="netinst-stable">공식 netinst 이미지 <q>안정</q> 릴리스 용</h2>

<p>최대 300&nbsp;MB 크기, 이 이미지는 설치관리자와 작은 패키지 세트를
포함하며 (매우) 기본적인 시스템 설치를 허용합니다.</p>

<div class="line">
<div class="item col50">
<p><strong>netinst CD 이미지 (<a href="$(HOME)/CD/torrent-cd">bittorrent</a>) 통해</strong></p>
	  <stable-netinst-torrent />
</div>
<div class="item col50 lastcol">
<p><strong>netinst CD 이미지 (대개 150-300 MB, 아키텍처에 따름)</strong></p>
	  <stable-netinst-images />
</div>
<div class="clear"></div>
</div>

<p>이들 파일이 무엇인지 그리고 그것을 어떻게 쓰는지는 <a href="../faq/">FAQ</a>를 보세요.</p>

<p>이미지를 다운로드하면, <a 
href="$(HOME)/releases/stable/installmanual">설치 과정에 대한 자세한 정보</a>를 확인하세요.</p>

<h2><a name="firmware">비 자유 펌웨어가 들어간 비공식 netinst 이미지</a></h2>

<div id="firmware_nonfree" class="important">
<p>
시스템의 하드웨어에 장치 드라이버에 <strong>비 자유 펌웨어 로드가 필요</strong>하면 
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
공통 펌웨어 패키지의 tarball</a> 중 하나를 사용 하거나 
이러한 <strong>비 자유</strong> 펌웨어를 포함한 <strong>비공식</strong> 이미지를 다운로드 할 수 있습니다.
tarball을 사용하는 방법에 대한 지침과 설치 중 펌웨어로드에 대한 일반 정보는 
<a href="../../releases/stable/amd64/ch06s04">설치 안내서</a>에 있습니다.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">\
펌웨어 들어간 <q>안정</q> 비공식 설치 이미지</a>
</p>
</div>
