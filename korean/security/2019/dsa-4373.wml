#use wml::debian::translation-check translation="08e97d6a66338b9fb8da51eb27b4c3dde971c164" maintainer="Sebul" mindelta="-1"
# 주의: 불완전한 번역. 번역을 마친 다음 위의 'mindelta="-1"'을 지우십시오.
<define-tag description>보안 업데이트</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were discovered in coTURN, a TURN and STUN server for
VoIP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-4056">CVE-2018-4056</a>

    <p>An SQL injection vulnerability was discovered in the coTURN administrator
    web portal. As the administration web interface is shared with the
    production, it is unfortunately not possible to easily filter outside
    access and this security update completely disable the web interface. Users
    should use the local, command line interface instead.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-4058">CVE-2018-4058</a>

    <p>Default configuration enables unsafe loopback forwarding. A remote attacker
    with access to the TURN interface can use this vulnerability to gain access
    to services that should be local only.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-4059">CVE-2018-4059</a>

    <p>Default configuration uses an empty password for the local command line
    administration interface. An attacker with access to the local console
    (either a local attacker or a remote attacker taking advantage of
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-4058">CVE-2018-4058</a>)
    could escalade privileges to administrator of the coTURN
    server.</p></li>

</ul>

<p>안정 배포(stretch)에서, 이 문제를 버전 4.5.0.5-1+deb9u1에서 수정했습니다.</p>

<p>coturn 패키지를 업그레이드 하는 게 좋습니다.</p>

<p>자세한 보안 상태는 보안 추적 페이지를 참조하십시오:
<a href="https://security-tracker.debian.org/tracker/coturn">\
https://security-tracker.debian.org/tracker/coturn</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4373.data"
