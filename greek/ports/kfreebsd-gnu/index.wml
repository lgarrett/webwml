#use wml::debian::template title="Debian GNU/kFreeBSD"
#use wml::debian::translation-check translation="6f3adf6374f35194686f89dec2ba66b1ecf3bb5f" maintainer="galaxico"

#use wml::debian::toc

<toc-display/>

<p>Debian GNU/kFreeBSD is a port that consists of
<a href="https://www.gnu.org/">GNU userland</a> using the
<a href="https://www.gnu.org/software/libc/">GNU C library</a> on top of
<a href="https://www.freebsd.org/">FreeBSD</a>'s kernel, coupled with the
regular <a href="https://packages.debian.org/">Debian package set</a>.</p>

<div class="important">
<p>Debian GNU/kFreeBSD is not an officially supported
architecture. It has been released with Debian 6.0 (Squeeze) and 7.0
(Wheezy) as a <em>technology preview</em> and the first non-Linux
port. Since Debian 8 (Jessie) it is though no more included in official
releases.</p>
</div>

<toc-add-entry name="resources">Resources</toc-add-entry>

<p>There's more information about the port (including an FAQ) in the
<a href="https://wiki.debian.org/Debian_GNU/kFreeBSD">Debian GNU/kFreeBSD</a>
wiki page.
</p>

<h3>Mailing lists</h3>
<p><a href="https://lists.debian.org/debian-bsd">Debian GNU/k*BSD mailing list</a>.</p>
<h3>IRC</h3>
<p><a href="irc://irc.debian.org/#debian-kbsd">#debian-kbsd IRC channel</a> (at irc.debian.org).</p>

<toc-add-entry name="Development">Development</toc-add-entry>

<p>Because we use Glibc the portability problems are very simple and most times it's
just a matter of copying a test case for "k*bsd*-gnu" from another Glibc-based
system (like GNU or GNU/Linux).  Look at the
<a href="https://salsa.debian.org/bsd-team/web/raw/master/porting/PORTING">porting</a>
document for details.</p>

<p>Also look at the <a href="https://salsa.debian.org/bsd-team/web/raw/master/TODO">TODO</a>
file for more details on what needs to be done.</p>

<toc-add-entry name="availablehw">Available Hardware for Debian Developers</toc-add-entry>

<p>lemon.debian.net (kfreebsd-amd64) is
available to Debian developers for porting work.  Please see the 
<a href="https://db.debian.org/machines.cgi">machine database</a> for more
information about these machines.  In general, you will be able to use the
two chroot environments: testing and unstable. Note that these systems 
are not administrated by DSA, so <b>do not send requests to debian-admin
about it</b>. Instead use <email "admin@lemon.debian.net">.</p>
