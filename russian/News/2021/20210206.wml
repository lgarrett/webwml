#use wml::debian::translation-check translation="156615cc19b61bffcfb93a5ff5e5e300fcbc9492" maintainer="Lev Lamberov"
<define-tag pagetitle>Обновлённый Debian 10: выпуск 10.8</define-tag>
<define-tag release_date>2021-02-06</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.8</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Проект Debian с радостью сообщает о восьмом обновлении своего
стабильного выпуска Debian <release> (кодовое имя <q><codename></q>).
Это обновление в основном содержит исправления проблем безопасности,
а также несколько корректировок серьёзных проблем. Рекомендации по безопасности
опубликованы отдельно и указываются при необходимости.</p>

<p>Заметьте, что это обновление не является новой версией Debian
<release>, а лишь обновлением некоторых включённых в выпуск пакетов. Нет
необходимости выбрасывать старые носители с выпуском <q><codename></q>. После установки
пакеты можно обновить до текущих версий, используя актуальное
зеркало Debian.</p>

<p>Тем, кто часто устанавливает обновления с security.debian.org, не придётся
обновлять много пакетов, большинство обновлений с security.debian.org
включены в данное обновление.</p>

<p>Новые установочные образы будут доступны позже в обычном месте.</p>

<p>Обновление существующих систем до этой редакции можно выполнить с помощью
системы управления пакетами, используя одно из множества HTTP-зеркал Debian.
Исчерпывающий список зеркал доступен на странице:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Исправления различных ошибок</h2>

<p>Данное стабильное обновление вносит несколько важных исправлений для следующих пакетов:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction atftp "Исправление отказа в обслуживании [CVE-2020-6097]">
<correction base-files "Обновление /etc/debian_version в соответствии с редакцией 10.8">
<correction ca-certificates "Обновление набора сертификатов Mozilla до версии 2.40, внесение просроченных сертификатов <q>AddTrust External Root</q> в чёрный список">
<correction cacti "Исправление SQL-инъекции [CVE-2020-35701] и проблемы с сохранённым XSS">
<correction cairo "Исправление использование маски в image-compositor [CVE-2020-35492]">
<correction choose-mirror "Обновление списка зеркал">
<correction cjson "Исправление бесконечного цикла в cJSON_Minify">
<correction clevis "Исправление создания initramfs; clevis-dracut: включение создания initramfs при установке">
<correction cyrus-imapd "Исправление сравнения версий в cron-сценарии">
<correction debian-edu-config "Перемещение кода очистки keytab-узлов из gosa-modify-host в отдельный сценарий, что снижает число LDAP-вызовов до одного запроса">
<correction debian-installer "Использование ABI ядра Linux версии 4.19.0-14; повторная сборка с учётом proposed-updates">
<correction debian-installer-netboot-images "Повторная сборка с учётом proposed-updates">
<correction debian-installer-utils "Поддержка разделов на устройствах USB UAS">
<correction device-tree-compiler "Исправление ошибки сегментирования при вызове <q>dtc -I fs /proc/device-tree</q>">
<correction didjvu "Добавление отсутствующей сборочной зависимости от tzdata">
<correction dovecot "Исправление аварийной остановки при поиске по почтовым ящикам, содержащим некорректные MIME-сообщения">
<correction dpdk "Новый стабильный выпуск основной ветки разработки">
<correction edk2 "CryptoPkg/BaseCryptLib: исправление разыменования NULL-указателя [CVE-2019-14584]">
<correction emacs "Исправление аварийной остановки, возникающей если пользовательские идентификаторы OpenPGP не имеют адресов электронной почты">
<correction fcitx "Исправление поддержки метода ввода в Flatpak">
<correction file "Увеличение глубины рекурсии по умолчанию при разборе имени до 50">
<correction geoclue-2.0 "Проверка максимально разрешённого уровня точности даже для системных приложений; предоставление возможности настройки API-ключа Mozilla и использование по умолчанию ключа Debian; исправление отображения индикатора использования">
<correction gnutls28 "Исправление ошибки тестового набора, вызванной просроченным сертификатом">
<correction grub2 "Выход при обновлении grub-pc в отличном от интерактивного режиме в случае, если grub-install выполнился с ошибкой; явная проверка существования целевого устройства до запуска grub-install; grub-install: добавление резервного копирования и восстановления; не запускать grub-install при свежей установке grub-pc">
<correction highlight.js "Исправление загрязнения прототипа [CVE-2020-26237]">
<correction intel-microcode "Обновление различных микропрограмм">
<correction iproute2 "Исправление ошибок в выводе JSON; исправление состояния гонки, приводящее к отказу в обслуживании системы при использовании ip netns add в ходе загрузки">
<correction irssi-plugin-xmpp "Отключение преждевременного срабатывания превышения времени ожидания соединения irssi для исправление соединений STARTTLS">
<correction libdatetime-timezone-perl "Обновлении до новой версии tzdata">
<correction libdbd-csv-perl "Исправление ошибки теста при использовании libdbi-perl 1.642-1+deb10u2">
<correction libdbi-perl "Исправление безопасности [CVE-2014-10402]">
<correction libmaxminddb "Исправление чтения за пределами выделенного буфера памяти [CVE-2020-28241]">
<correction lttng-modules "Исправление сборки при использовании ядра с версией &gt;= 4.19.0-10">
<correction m2crypto "Исправление совместимости с OpenSSL 1.1.1i и более новых версий">
<correction mini-buildd "builder.py: вызов sbuild: явное использование опции '--no-arch-all'">
<correction net-snmp "snmpd: добавление флагов cacheTime и execType к EXTEND-MIB">
<correction node-ini "Запрет использования некорректных потенциально опасных строк в качестве имени раздела [CVE-2020-7788]">
<correction node-y18n "Исправление загрязнения прототипа [CVE-2020-7774]">
<correction nvidia-graphics-drivers "Новый выпуск основной ветки разработки; исправление потенциальных отказа в обслуживании и раскрытия информации [CVE-2021-1056]">
<correction nvidia-graphics-drivers-legacy-390xx "Новый выпуск основной ветки разработки; исправление потенциальных отказа в обслуживании и раскрытия информации [CVE-2021-1056]">
<correction pdns "Исправления безопасности [CVE-2019-10203 CVE-2020-17482]">
<correction pepperflashplugin-nonfree "Перевод в статус пустого пакета, удаляющего ранее установленное дополнение (оно более не работает и не поддерживается)">
<correction pngcheck "Исправление переполнения буфера [CVE-2020-27818]">
<correction postgresql-11 "Новый стабильный выпуск основной ветки разработки; исправления безопасности [CVE-2020-25694 CVE-2020-25695 CVE-2020-25696]">
<correction postsrsd "Выполнение проверки того, что тэги временных меток не являются слишком длинными, до того, как запускается их декодирование [CVE-2020-35573]">
<correction python-bottle "Запрет на использование <q>;</q> в качестве разделителя сторок-запросов [CVE-2020-28473]">
<correction python-certbot "Автоматическое использование API ACMEv2 для обновления сертификатов, чтобы избежать проблем с удалением API ACMEv1">
<correction qxmpp "Исправление потенциальной ошибки сегментирования при ошибке соединения">
<correction silx "python(3)-silx: добавление зависимости от python(3)-scipy">
<correction slirp "Исправление переполнений буфера [CVE-2020-7039 CVE-2020-8608]">
<correction steam "Новый выпуск основной ветки разработки">
<correction systemd "journal: не инициировать утверждение, если функции journal_file_close() передан NULL-указатель">
<correction tang "Обход состояния гонки между созданием ключа и обновлением">
<correction tzdata "Новый выпуск основной ветки разработки; обновление поставляемых данных о часовых поясах">
<correction unzip "Применение дополнительных исправлений для CVE-2019-13232">
<correction wireshark "Исправление аварийных остановок, бесконечных циклов и утечек информации [CVE-2019-16319 CVE-2019-19553 CVE-2020-11647 CVE-2020-13164 CVE-2020-15466 CVE-2020-25862 CVE-2020-25863 CVE-2020-26418 CVE-2020-26421 CVE-2020-26575 CVE-2020-28030 CVE-2020-7045 CVE-2020-9428 CVE-2020-9430 CVE-2020-9431]">
</table>


<h2>Обновления безопасности</h2>


<p>В данный выпуск внесены следующие обновления безопасности. Команда
безопасности уже выпустила рекомендации для каждого
из этих обновлений:</p>

<table border=0>
<tr><th>Идентификационный номер рекомендации</th>  <th>Пакет</th></tr>
<dsa 2020 4797 webkit2gtk>
<dsa 2020 4801 brotli>
<dsa 2020 4802 thunderbird>
<dsa 2020 4803 xorg-server>
<dsa 2020 4804 xen>
<dsa 2020 4805 trafficserver>
<dsa 2020 4806 minidlna>
<dsa 2020 4807 openssl>
<dsa 2020 4808 apt>
<dsa 2020 4809 python-apt>
<dsa 2020 4810 lxml>
<dsa 2020 4811 libxstream-java>
<dsa 2020 4812 xen>
<dsa 2020 4813 firefox-esr>
<dsa 2020 4814 xerces-c>
<dsa 2020 4815 thunderbird>
<dsa 2020 4816 mediawiki>
<dsa 2020 4817 php-pear>
<dsa 2020 4818 sympa>
<dsa 2020 4819 kitty>
<dsa 2020 4820 horizon>
<dsa 2020 4821 roundcube>
<dsa 2021 4822 p11-kit>
<dsa 2021 4823 influxdb>
<dsa 2021 4824 chromium>
<dsa 2021 4825 dovecot>
<dsa 2021 4827 firefox-esr>
<dsa 2021 4828 libxstream-java>
<dsa 2021 4829 coturn>
<dsa 2021 4830 flatpak>
<dsa 2021 4831 ruby-redcarpet>
<dsa 2021 4832 chromium>
<dsa 2021 4833 gst-plugins-bad1.0>
<dsa 2021 4834 vlc>
<dsa 2021 4835 tomcat9>
<dsa 2021 4837 salt>
<dsa 2021 4838 mutt>
<dsa 2021 4839 sudo>
<dsa 2021 4840 firefox-esr>
<dsa 2021 4841 slurm-llnl>
<dsa 2021 4843 linux-latest>
<dsa 2021 4843 linux-signed-amd64>
<dsa 2021 4843 linux-signed-arm64>
<dsa 2021 4843 linux-signed-i386>
<dsa 2021 4843 linux>
</table>


<h2>Удалённые пакеты</h2>

<p>Следующие пакеты были удалены из-за обстоятельств, на которые мы не
можем повлиять:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction compactheader "Несовместим с текущими версиями Thunderbird">

</table>

<h2>Программа установки Debian</h2>

Программа установки была обновлена с целью включения исправлений, добавленных в
данную редакцию стабильного выпуска.

<h2>URL</h2>

<p>Полный список пакетов, которые были изменены в данной редакции:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Текущий стабильный выпуск:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Предлагаемые обновления для стабильного выпуска:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Информация о стабильном выпуске (информация о выпуске, известные ошибки и т. д.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Анонсы безопасности и информация:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>О Debian</h2>

<p>Проект Debian &mdash; объединение разработчиков свободного программного обеспечения,
которые жертвуют своё время и знания для создания абсолютно свободной
операционной системы Debian.</p>

<h2>Контактная информация</h2>

<p>Более подробную информацию вы можете получить на сайте Debian
<a href="$(HOME)/">https://www.debian.org/</a>, либо отправив письмо по адресу
&lt;press@debian.org&gt;, либо связавшись с командой стабильного выпуска по адресу
&lt;debian-release@lists.debian.org&gt;.</p>
