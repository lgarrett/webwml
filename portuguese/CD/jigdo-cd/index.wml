#use wml::debian::cdimage title="Baixando imagens do CD Debian com o jigdo" BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="a7e5f7cb49aa3db66a76fbe2aeb7c9a69f02ae0c"

<p>Jigsaw Download, ou resumindo <a
href="http://atterer.org/jigdo/">jigdo</a>, é
uma maneira fácil e amigável de distribuir imagens de CD e DVD Debian.</p>

<toc-display/>

<toc-add-entry name="why">Por que o jigdo é melhor que um download?
direto</toc-add-entry>

<p>Porque é mais rápido! Por várias razões, há um número bem menor de
servidores espelhos para as imagens de CD do que para o repositório "normal" do
Debian. Consequentemente se você baixar de um servidor espelho das imagens de
CD, esse servidor não só estará mais distante de você como também estará
sobrecarregado, especialmente logo após o lançamento de uma versão.</p>

<p>Além disso, alguns tipos de imagens não estão disponíveis como arquivos
<tt>.iso</tt> completos para download, pois não há espaço suficiente em nossos
servidores para hospedá-los.</p>

<p>É claro, um servidor espelho Debian "normal" não tem nenhuma imagem de CD,
então como é que o jigdo consegue baixá-las de lá? O jigdo faz isso baixando
individualmente todos os arquivos que estão no CD. Na etapa seguinte, todos
estes arquivos são agregados em um único grande arquivo que é a cópia exata da
imagem do CD ou DVD. De qualquer forma, tudo isso acontece em segundo plano -
tudo que <em>você</em> deve fazer é informar, à ferramenta que baixa os
arquivos, a localização de um arquivo "<tt>.jigdo</tt>" para ser processado.</p>

<p>Mais informações estão disponíveis na
<a href="http://atterer.org/jigdo/">página do jigdo</a>.
Voluntários(as) querendo ajudar com o desenvolvimento do jigdo são sempre
bem-vindos(as)!</p>

<toc-add-entry name="how">Como baixar uma imagem com o jigdo</toc-add-entry>

<ul>

  <li>Baixe um pacote contendo o <tt>jigdo-lite</tt>. Ele está
  diretamente disponível para instalação nas distribuições Debian e
  Ubuntu no pacote <tt>jigdo-file</tt>. No FreeBSD,
  instale a partir de /usr/ports/net-p2p/jigdo ou obtenha o pacote
  com <tt>pkg_add -r jigdo</tt>. Para outras opções de instalação
  (binários para Windows, código-fonte), consulte a
  <a href="http://atterer.org/jigdo/">página do
  jigdo</a>. </li>

  <li>Execute o script <tt>jigdo-lite</tt>. Ele vai pedir a URL
  de um arquivo "<tt>.jigdo</tt>" para processar. Caso queira, você também pode
  fornecer esta URL na linha de comando.</li>

  <li>A partir de um dos locais listados <a href="#which">abaixo</a>,
  escolha os arquivos "<tt>.jigdo</tt>" que você quer baixar e informe suas
  URLs no prompt do <tt>jigdo-lite</tt>. Cada arquivo "<tt>.jigdo</tt>"
  corresponde a uma imagem "<tt>.iso</tt>" de CD/DVD.</li>

  <li>Caso você seja um(a) usuário(a) iniciante, somente pressione Enter no
  prompt "Files to scan".</li>

  <li>No prompt "Debian mirror", informe
  <kbd>http://deb.debian.org/debian/</kbd> ou
  <kbd>http://ftp.<strong><var>XY</var></strong>.debian.org/debian/</kbd>, onde
  <strong><var>XY</var></strong> é o código de duas letras para seu
  país (por exemplo, <tt>br</tt>, <tt>us</tt>, <tt>uk</tt>. Consulte a
  lista atual de <a href="$(HOME)/mirror/list">locais
  ftp.<var>XY</var>.debian.org disponíveis</a>).

  <li>Siga as instruções dadas pelo programa. Se tudo correr bem, ele termina
  com o cálculo do checksum da imagem gerada e lhe informa que este checksum
  é o mesmo da imagem original.</li>

</ul>

<p>Para uma descrição detalhada e passo a passo desse processo, consulte
o <a href="http://www.tldp.org/HOWTO/Debian-Jigdo/">mini-HOWTO Debian
jigdo</a>. O HOWTO também explica os recursos avançados do jigdo, como a
atualização de uma versão antiga da imagem de um CD ou DVD para a versão atual
(baixando somente o que foi alterado, não toda a imagem).</p>

<p>Uma vez que você tenha baixado as imagens e gravado em um CD/DVD,
consulte as <a
href="$(HOME)/releases/stable/installmanual">informações detalhadas sobre o
processo de instalação</a>.</p>

<toc-add-entry name="which">Imagens oficiais</toc-add-entry>

<h3>Arquivos jigdo oficiais para a distribuição estável (<q>stable</q>)</h3>

<div class="line">
<div class="item col50">
<p><strong>CD</strong></p>
  <stable-full-cd-jigdo />
</div>
<div class="item col50 lastcol">
<p><strong>DVD</strong></p>
  <stable-full-dvd-jigdo />
</div>
<div class="clear"></div>
</div>
<div class="line">
<div class="item col50">
<p><strong>Blu-ray</strong></p>
  <stable-full-bluray-jigdo />
</div>
</div>

<p>Tenha certeza de ter consultado a documentação antes de instalar.
<strong>Se você prefere ler apenas um documento</strong> antes da instalação, leia
nosso <a href="$(HOME)/releases/stable/i386/apa">howto de instalação</a>, um rápido
passo a passo do processo de instalação. Outras documentações úteis incluem:
</p>
<ul>
<li><a href="$(HOME)/releases/stable/installmanual">Guia de Instalação</a>,
    as instruções de instalação detalhadas</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Documentação do
    instalador do Debian</a>, incluindo a FAQ com dúvidas comuns e respostas</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">Errata do
    instalador do Debian</a>, a lista de problemas conhecidos no instalador</li>
</ul>

<h3>Arquivos jigdo oficiais para a distribuição teste (<q>testing</q>)</h3>
<div class="line">
<div class="item col50">
<p><strong>CD</strong></p>
  <devel-full-cd-jigdo />
</div>
<div class="item col50 lastcol">
<p><strong>DVD</strong></p>
  <devel-full-dvd-jigdo />
</div>
</div>

<hr />

<toc-add-entry name="search">Procurar conteúdo das imagens de CD</toc-add-entry>

<p><strong>Qual imagem de CD/DVD contém um determinado arquivo?</strong> Abaixo,
você pode fazer uma busca nas listas de arquivos contidos em uma ampla variedade
de imagens de CD/DVD do Debian. Você pode entrar com várias palavras, cada uma
delas deve coincidir com um trecho do nome do arquivo. Adicione, por exemplo,
"_i386" para restringir os resultados a uma determinada arquitetura. Adicione
"_all" para visualizar pacotes que são idênticos para todas arquiteturas.</p>

<form method="get" action="https://cdimage-search.debian.org/"><p>
<input type="hidden" name="search_area" value="release">
<input type="hidden" name="type" value="simple">
<input type="text" name="query" size="20" value="">
# Tradutores(as): "Search" (Pesquisar) deve ser traduzida.
<input type="submit" value="Pesquisar"></p></form>

<p><strong>Quais arquivos estão contidos em uma determinada imagem?</strong> Se
você precisa de uma lista com <em>todos</em> os arquivos que um determinado
CD/DVD do Debian contém, basta consultar o arquivo <tt>list.gz</tt>
correspondente da imagem em <a
href="https://cdimage.debian.org/cdimage/">cdimage.debian.org</a>.</p>

<hr>

<toc-add-entry name="faq">Dúvidas frequentes</toc-add-entry>

<p><strong>Como faço para o jigdo usar meu proxy?</strong></p>

<p>Carregue o arquivo <tt>~/.jigdo-lite</tt> (ou o
<tt>jigdo-lite-settings.txt</tt> para a versão Windows) em um editor de texto e
procure a linha que começa com "wgetOpts". As opções a seguir podem ser
adicionadas à linha:</p>

<p><tt>-e ftp_proxy=http://<i>PROXY-LOCAL</i>:<i>PORTA</i>/</tt>
<br><tt>-e http_proxy=http://<i>PROXY-LOCAL</i>:<i>PORTA</i>/</tt>
<br><tt>--proxy-user=<i>USUÁRIO</i></tt>
<br><tt>--proxy-passwd=<i>SENHA</i></tt></p>

<p>É claro, substitua os valores corretos para o seu servidor proxy. As
duas últimas opções só são necessárias se o seu proxy utiliza
autenticação com senha. As opções devem ser adicionadas ao final da linha
wgetOpts <em>antes</em> do caractere final <tt>'</tt>. Todas as opções
devem estar numa única linha.</p>

<p>Alternativamente, no Linux você também pode configurar as variáveis
de ambiente <tt>ftp_proxy</tt> e <tt>http_proxy</tt>, por exemplo no
arquivo <tt>/etc/environment</tt> ou no
<tt>~/.bashrc</tt>.</p>

<p><strong>Aargh! O programa parou com um erro - será que baixei todos
aqueles megabytes em vão?!</strong></p>

<p>É claro que Isso Não Deveria Ter Acontecido((tm), mas por várias razões
você pode ficar numa situação em que um grande arquivo <q><tt>.iso.tmp</tt></q>
já foi gerado e parece que o <tt>jigdo-lite</tt> está com problemas,
dizendo repetidamente para você baixar novamente os arquivos. Há várias
possibilidades que podem ser testadas neste caso:</p>

<ul>

  <li>Simplesmente reiniciar a cópia pressionando Enter. Talvez alguns dos
  arquivos não tenham sido baixados por causa de "timeouts" (tempo esgotado) ou
  outros erros temporários - será realizada outra tentativa de baixar quaisquer
  arquivos que estejam faltando.</li>

  <li>Tente um servidor espelho ("mirror") diferente. Alguns servidores espelho
  Debian estão ligeiramente fora de sincronia - talvez um servidor diferente ainda
  tenha os arquivos que foram apagados daquele que você especificou, ou já foi
  atualizado com arquivos que ainda não estão presentes no seu servidor espelho.</li>

  <li>Baixar as partes da imagem que estão faltando usando o <tt>
  <a href="https://rsync.samba.org/">rsync</a></tt>. Primeiro, você precisa
  achar a URL do rsync correta para a imagem que está baixando:
  Escolha um servidor que ofereça acesso rsync para as imagens
  <a href="../mirroring/rsync-mirrors">estável</a> (<q>stable</q>) ou
  <a href="../http-ftp/#testing">teste</a> (<q>testing</q>), então determine o
  caminho correto e o nome do arquivo. Listagens de diretórios podem ser
  obtidas com comandos do tipo
  <tt>rsync&nbsp;rsync://cdimage.debian.org/debian-cd/</tt>

  <br>A seguir, remova a extensão "<tt>.tmp</tt>" do arquivo
  <tt>jigdo-lite</tt> temporário, renomeando-o, e passe ambos,
  a URL remota e o nome do arquivo local para o rsync:
  <tt>rsync&nbsp;rsync://server.org/path/binary-i386-1.iso
  binary-i386-1.iso</tt>

  <br>Você pode usar as opções <tt>--verbose</tt> e <tt>--progress</tt>
  do rsync para obter mensagens de estado, e <tt>--block-size=8192</tt> para
  aumentar a velocidade.</li>

  <li>Se tudo isso falhar, seus dados baixados ainda não estão perdidos:
  no Linux, você pode montar o arquivo <tt>.tmp</tt> como um dispositivo "loop"
  para acessar os pacotes que já foram baixados, e reutilizá-los para
  gerar uma imagem de um novo arquivo jigdo (tal como a imagem semanal de
  teste <q>testing</q> se sua cópia que falhou for também uma imagem teste (<q>testing</q>)).
  Para fazer isso, primeiro execute os seguintes comandos como "root" no diretório
  com o arquivo baixado incompleto: <tt>mkdir&nbsp;mnt;
  mount&nbsp;-t&nbsp;iso9660&nbsp;-o&nbsp;loop&nbsp;*.tmp&nbsp;mnt</tt>.
  A seguir, inicie uma nova cópia num diretório diferente, e informe o caminho
  do diretório <tt>mnt</tt> quando for solicitado no prompt "Files to
  scan".</li>

</ul>
