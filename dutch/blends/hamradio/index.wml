#use wml::debian::blend title="Specifieke uitgave van Debian voor amateurradio" NOHEADER="true" BLENDHOME="true"
#use wml::debian::recent_list
#use wml::debian::blends::hamradio
#use wml::debian::translation-check translation="6d2e0afb56e760364713c2cca2c9f6407c9a744f"

<span class="download"><a href="<stable-amd64-url/>">
Debian amateurradio downloaden <stable-version/><br/><em>(64-bits Live-dvd)</em></a>
</span>
<div id="splash">
	<h1 id="hamradio">Specifieke uitgave van Debian voor amateurradio</h1>
</div>

<p>De <b>Specifieke uitgave van Debian voor amateurradio</b> is een project van
het <a href="https://wiki.debian.org/DebianHams/">Debian ontwikkelaarsteam voor
amateurradio</a> dat samenwerkt aan het onderhouden van pakketten voor Debian
die verband houden met amateurradio.
Elke <a href="https://blends.debian.org/">Specifieke uitgave</a> is een
deelverzameling van Debian welke werd geconfigureerd om een bepaalde doelgroep
met een gebruiksklaar product te ondersteunen. Deze specifieke uitgave heeft tot
doel tegemoet te komen aan de behoeften van radioamateurs.</p>

<p><a href="./about">Meer lezen&hellip;</a></p>

<div id="hometoc">
<ul id="hometoc-cola">
  <li><a href="./about">Over de specifieke uitgave</a></li>
  <li><a href="./News/">Nieuwsarchieven</a></li>
  <li><a href="./contact">Ons contacteren</a></li>
</ul>
<ul id="hometoc-colb">
  <li><a href="./get/">De specifieke uitgave verkrijgen</a>
  <ul>
    <li><a href="./get/live">live-images downloaden</a></li>
    <li><a href="./get/metapackages">Gebruik maken van de metapakketten</a></li>
  </ul></li>
</ul>
<ul id="hometoc-colc">
  <li><a href="./docs">Documentatie</a>
  <ul>
    <li><a href="<hamradio-handbook-html/>">Debian amateurradiohandboek</a></li>
  </ul></li>
  <li><a href="./support">Ondersteuning</a></li>
</ul>
<ul id="hometoc-cold">
  <li><a href="./dev">Ontwikkeling</a>
    <ul>
      <li><a href="https://wiki.debian.org/DebianHams">Ontwikkelaarsteam voor
amateurradio</a></li>
      <li><a href="<hamradio-maintguide-html/>">Ontwikkelaarshandleiding voor
amateurradio</a></li>
    </ul>
  </li>
</ul>
<ul id="hometoc-cole">
  <li><a href="./fun">Pret</a></li>
  <li><a href="https://twitter.com/DebianHamradio"><img src="Pics/twitter.gif" alt="Twitter" width="80" height="15" /></a></li>
</ul>
</div>

<h2>Aan de slag</h2>

<ul>
<li>Als u Debian al hebt geïnstalleerd, bekijk dan de
<a href="./get/metapackages">lijst met metapakketten</a> om amateurradiosoftware
te ontdekken die u kunt installeren.</li>
<li>Als Debian nieuw voor u is, kunt u een <a href="./get/live">live-image</a>
uitproberen waarmee u de amateurradio-uitgave van Debian kunt ervaren zonder
wijzigingen aan uw systeem aan te brengen. Het bevat ook een
installatieprogramma, zodat u het op uw systeem kunt installeren als het u
bevalt.</li>
<li>Om hulp te krijgen bij het gebruik van amateurradiosoftware in Debian, kunt
u een van onze <a href="./support">ondersteuningskanalen</a> gebruiken.</li>
</ul>

<h2>Nieuws</h2>

<p><:= get_recent_list('News/$(CUR_YEAR)', '6',
'$(ENGLISHDIR)/blends/hamradio', '', '\d+\w*' ) :>
</p>

