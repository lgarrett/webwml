#use wml::debian::template title="Beleid in verband met het gebruik van machines van Debian" NOHEADER=yes
#use wml::debian::translation-check translation="b8114b588961778dbd04974c1464a2f388a90c28"

<h2>Beleid in verband met het gebruik van machines van Debian"</h2>
<p>
Versie 1.1.2<br>
Deze versie van het beleid in verband met het gebruik van machines van Debian
treedt in voege op 4 juli 2010 en vervangt alle vorige
<a href="dmup.1.1.1">versies</a>. Ze werd <a
href="https://lists.debian.org/debian-devel-announce/2010/05/msg00001.html">aangekondigd
op 9 mei 2010</a>.</p>

<ol>
<li><strong>Inleiding</strong><br>

Dit document beschrijft het beleid in verband met het gebruik van
<a href="https://db.debian.org/machines.cgi">machines van Debian</a> en
alle regels eromheen.

<p>In het kort:
<ul>
<li>Het systeembeheerdersteam van Debian zal alles doen wat nodig is om alle
    machines en services op een veilige manier te laten werken en draaien.

<li>Voer geen bewuste, opzettelijke, roekeloze of onwettige handelingen uit
    die het werk van andere ontwikkelaars verstoren of de integriteit van
    gegevensnetwerken, computerapparatuur, systeemprogramma's of andere
    opgeslagen informatie in gevaar brengen.

<li>Maak geen gebruik van voorzieningen van Debian voor eigen financieel gewin
    of voor commerciële doeleinden met inbegrip van  adviesverstrekking of enig
    ander werk dat voorlopig buiten het bereik van de officiële taken of
    functies valt, zonder specifieke toestemming om dit te doen.

<li>Maak geen gebruik van voorzieningen van Debian voor onwettige activiteiten,
    inclusief maar niet beperkt tot softwarepiraterij.
</ul>

<p>Dit document bestaat uit twee delen: beleidslijnen en richtlijnen. De regels
uit de beleidslijnen zijn bindend en mogen niet overtreden worden. De
richtlijnen specificeren regels die indien nodig overtreden kunnen worden,
maar we zouden liever hebben dat men dit niet deed.


<li><strong>Algemene verklaringen</strong><br>

<ol>
<li> Gebruikte afkortingen<br>
  <ul>
  <li>DSA   - Debian Systems Administration (Debian systeembeheer)
  <li>DMUP  - Debian Machine Usage Policy (dit document) (Debian beleid voor machinegebruik)
  <li>DPL   - Debian Project Leader (Debian projectleider)
  <li>DAM   - Debian Account Managers (Debian accountbeheerders)
  </ul>

<li> Voorrecht<br>
Toegang tot de voorzieningen van Debian is een voorrecht, geen recht of geen
commerciële dienst en het DSA behoudt zich het recht voor om dit voorrecht op
elk moment in te trekken zonder voorafgaande kennisgeving. Binnen de 48 uur zal
daarover toelichting gegeven worden.

<li> Waarborgen<br>
Er is geen garantie op de dienst. Hoewel het DSA zijn best zal doen om te verzekeren dat alles perfect functioneert, kan het geen enkele garantie geven.

<li> Privacy<br>
Als dit noodzakelijk is om het behoorlijk functioneren van machines te kunnen
blijven garanderen, is het DSA toegestaan om bestanden van gebruikers te
bewerken (bijvoorbeeld .forward-bestanden aanpassen om e-maillussen te
doorbreken.)
</ol>

<li><strong>Sancties</strong><br>

Indien iemand de regels die in dit document vastgelegd worden, overtreedt, zal
die persoon een sanctie krijgen. De aard van de sanctie is afhankelijk van het
aantal eerdere overtredingen en van de aard van de overtreding.

<ol>
<li> Eerste overtreding<br>

<ol>
<li>De accounts van de overtreder zullen opgeschort worden en de betrokkene zal
    geen toegang meer hebben.

<li>De overtreder zal contact moeten opnemen met het DSA en het ervan overtuigen
    dat er geen verdere inbreuken op de DMUP meer gepleegd zullen worden.

<li>Indien de overtreder binnen de 14 dagen geen contact opgenomen heeft met het
    DSA zal het DSA het betreffende account schorsen en de uitsluiting van de
    overtreder uit het Debian-project voorstellen aan de DAM. Indien de
    overtreder aangekondigd had dat deze binnen dit tijdskader met vakantie zou
    zijn, zal deze periode verlengd worden met de aangekondigde duur van de
    vakantie.

<li>Indien de overtreder uitgesloten werd uit het project, kan deze zich na een
    maand registreren om opnieuw ontwikkelaar te worden. De overtreding blijft
    geregistreerd.
</ol>


<li> Tweede overtreding<br>

<ol>
<li>Het account van de overtreder zal onmiddellijk geschorst worden en het DSA
    zal aan de DAM de uitsluiting van de overtreder uit het Debian-project
    voorstellen.

<li>Indien de overtreder geen beroep aantekent binnen de aangewezen tijd, wordt
    het account afgesloten.

</ol>


<li> Publicatie<br>

<ol>
<li>De overtreding en de sanctie zullen enkel aan de ontwikkelaars van Debian
    bekendgemaakt worden.

<li>Mocht het naar het oordeel van de Debian-projectleider nodig worden
    geacht, dan zal een openbare kennisgeving gebeuren. Dit kan de identiteit
    van de overtreder omvatten.
</ol>


<li> Beroep<br>

<ol>
<li>Indien de overtreder zich niet akkoord kan verklaren met de beslissing van
    het DSA, kan deze bij de ontwikkelaars beroep aantekenen. Dit kan enkel
    gebeuren binnen de 14 dagen die onmiddellijk volgen op de dag waarop de
    overtreder geïnformeerd werd over de sanctie. Dit moet gebeuren volgens de
    procedure die in afdeling 4.2. van de statuten van Debian beschreven wordt.

<li>Gedurende de tijd dat het beroep in behandeling is, blijft het account
    geschorst.
</ol>
</ol>


<li><strong>De beleidslijnen</strong><br>

In deze afdeling worden de beleidslijnen vermeld. De opsomming is niet
omvattend en kan dit ook niet zijn.


<dl>
<dt>Schijfgebruik:

<dd>Op alle machines is een achtergronddienst actief die de map /tmp en
bestanden ouder dan een week opruimt. Sommige machines hebben een
/scratch-partitie die speciaal bedoeld is voor het opslaan van grote
gegevenssets zonder dat men bevreesd moet zijn dat deze gewist zullen worden.
Indien u een e-mailbericht ontvangt dat uw persoonlijke map erg groot is en dat
er meer vrije ruimte nodig is, onderneem dan onmiddellijk actie. Mogelijk acht
het DSA het nodig om op te schonen zonder vooraf te waarschuwen.

<dt>Shell:

<dd>Gebruik indien mogelijk ssh/scp in plaats van minder veilige alternatieven
(rsh, telnet of FTP).

<p>Inactieve verbindingen worden na een uur verbroken; dit is gemakkelijk te
omzeilen, maar doe dit niet zonder grondige reden.

<p>Het spiegelen via privé-middelen van gelijk welk deel van de openbare
archieven van de privéservers is strikt verboden zonder de voorafgaande
toestemming van de plaatselijke spiegelserverbeheerder (Mirror Master). Het
staat ontwikkelaars vrij om alle openbaar beschikbare vormen van toegang te
gebruiken.

<dt>Processen:

<dd>Voer geen langdurige processen uit zonder de toestemming van het DSA. Ook
het draaien van een server (onder meer IRC-bots) is verboden zonder
voorafgaande toestemming van het DSA. Vermijd het uitvoeren van processen die
misbruik maken van de CPU of het geheugen. Het DSA zal dergelijke processen
zonder waarschuwing opruimen als dit nodig is.

<dt>WWW-pagina's:

<dd>Over het algemeen wordt webruimte op Debian-machines voorzien om ideeën en
bestanden met betrekking tot het project of tot de vrije-softwaregemeenschap in
het algemeen te communiceren. Privé-pagina's op Debian-machines, die bedoeld
zijn om uw 'ijdelheid' te strelen, worden niet aangemoedigd.

<p>Commerciële webpagina's zijn niet toegestaan.

<p>U bent verantwoordelijk voor de inhoud van uw webpagina's, inclusief het
verkrijgen van de wettelijke toestemming voor alle werken die ze bevatten, en
ervoor te zorgen dat de inhoud van deze pagina's de wetten die van toepassing
zijn op de locatie van de server, niet schendt.

<p>U heeft en aanvaardt de verantwoordelijkheid voor lasterlijk, vertrouwelijk,
geheim of ander eigendomsrechtelijk materiaal dat beschikbaar is via uw
WWW-pagina's.

<p>U mag geen reclame maken voor uw WWW-pagina's, of ervoor zorgen dat iemand
anders er reclame voor maakt, met technieken die als misbruik zouden worden
aangemerkt als ze zouden worden uitgevoerd vanaf een Debian-account. Dit omvat,
maar is niet beperkt tot, bulkpost en buitensporig posten van nieuws. Een
dergelijke actie kan worden behandeld onder de passende DMUP alsof deze werd
uitgevoerd vanaf het account, of als een schending van deze DMUP of beide.

<dt>E-mail/Nieuws:

<dd>Een Debian machine gebruiken om e-mail te lezen is oké. Gebruik alstublieft
een weinig belaste machine. We ondersteunen niet het gebruik van
downloadmethoden voor e-mail, zoals POP of IMAP. Gebruik de mailserver van uw
ISP en de techniek van het doorsturen van e-mail. Net als bij webpagina's wordt
aangemoedigd dat inkomende e-mail over het algemeen in verband staat met vrije
software of op de een of andere manier aan het project gerelateerd is. Het DSA
kan het nodig vinden om zonder waarschuwing e-mail te comprimeren, te
verplaatsen of te wissen.
</dl>

<p>Als een ontwikkelaar voor langere tijd onbereikbaar wordt, kunnen diens
accounts, gegevens en het doorsturen/filteren van e-mail, enz. worden
uitgeschakeld totdat deze terug opduikt.


<p>Gebruik Debian-voorzieningen niet op een manier die misbruik van het net
inhoudt. Debian heeft geen Usenet-nieuwsservers. Het kan zijn dat sommige van
de Debian-machines toegang hebben tot een dergelijke nieuwsserver, maar het
gebruik ervan via Debian-machines is ten strengste verboden.

<p>Voorbeelden van wat DSA beschouwt als misbruik van het net:

<ul>
<li><em>Kettingbrieven en Ponzi-piramideverkopen</em><br>

          Dergelijke berichten werken (of liever, werken niet) op vrijwel
          dezelfde manier als hun verwanten op papier. Het meest voorkomende
          voorbeeld hiervan in e-mail is MAKKELIJK GELD VERDIENEN. Dergelijke
          berichten zijn niet alleen een verspilling van middelen, maar zijn
          ook in bepaalde landen illegaal.

<li><em>Commerciële e-mail waar men niet om gevraagd heeft</em><br>

          Ongevraagde commerciële e-mail is advertentiemateriaal dat per e-mail
          wordt ontvangen zonder dat de ontvanger om dergelijke informatie
          verzoekt of op een andere manier interesse toont in het geadverteerde
          materiaal.

          <p>Omdat veel internetgebruikers een inbelverbinding hebben en
          betalen voor de tijd dat ze online zijn, kost het ontvangen van
          e-mail hen geld. Ongevraagd commerciële reclame ontvangen kost hen
          geld en is daarom bijzonder onwelkom.

          <p>Opgemerkt moet worden dat een gebruiker die louter een
          nieuwsartikel in een bepaalde nieuwsgroep plaatst, daardoor geen
          blijk van interesse geeft, tenzij hij natuurlijk een specifiek
          verzoek doet om informatie te verkrijgen via e-mail.

<li><em>Ongevraagde bulke-mail</em><br>

          Vergelijkbaar met de bovenstaande ongevraagde commerciële e-mail,
          maar zonder dat getracht wordt om iets te verkopen. Meestal is lastig
          vallen de enige bedoeling.

<li><em>Vervalste kopregels en/of adressen</em><br>

          Het vervalsen van kopregels of berichten betekent het verzenden van
          e-mail op zo een manier dat het erop lijkt dat die afkomstig is van
          een andere gebruiker of computer of van een onbestaande computer.

          <p>Het is ook vervalsing om ervoor te zorgen dat alle antwoorden op
          een e-mail naar een andere gebruiker of machine worden gestuurd.

          <p>In beide gevallen is er echter geen probleem als u vooraf
          toestemming hebt gekregen van de andere gebruiker of van de
          beheerders van de andere machine, en natuurlijk kunnen zogenaamde
          "nul"-antwoordadressen gebruikt worden volgens de definiëringen in de
          relevante RFC's.

<li><em>E-mailbombardementen</em><br>

          Een e-mailbombardement is het verzenden van meerdere e-mails, of één
          grote e-mail, met als enige doel het lastig vallen van en/of wraak
          nemen op een andere internetgebruiker. Het is een verspilling van
          gemeenschappelijke internetbronnen en heeft ook geen waarde voor de
          ontvanger.

          <p>Vanwege de tijd die nodig is om deze te downloaden, kan het
          verzenden van lange e-mails naar sites zonder voorafgaande
          toestemming neerkomen op een zogenaamde
          <q>denial-of-service</q>-aanval. Merk op dat wanneer aan een e-mail
          binaire bijlagen toegevoegd worden, dit de omvang ervan aanzienlijk
          kan vergroten. Als daarover geen voorafgaande afspraak is gemaakt,
          zal de e-mail buitengewoon onwelkom zijn.

<li><em>Denial-of-service-aanvallen</em><br>

          Denial-of-service is elke activiteit die bedoeld is om te voorkomen
          dat een specifieke computer op internet volledig en effectief gebruik
          maakt van zijn mogelijkheden. Dit omvat, maar is niet beperkt tot:

          <ul>
          <li>Een adres bombarderen met e-mails op zo een manier dat de toegang
            tot het internet voor hen onmogelijk, moeilijk of duur gemaakt
            wordt.
          <li>Openen van een buitensporig aantal e-mailverbindingen met
            dezelfde computer.
          <li>Opzettelijk e-mail verzenden die bedoeld is om de systemen van de
            ontvanger te beschadigen wanneer deze geopend wordt, bijvoorbeeld
            kwaadwillige programma's of virussen in bijlage meesturen met een
            e-mail.
          <li>Het gebruik van een smarthost of SMTP-relais zonder dat daarvoor
            toestemming gegeven werd.
          </ul>

<li><em>Mailinglijst-abonnementen</em><br>

          U mag niemand, behalve een gebruiker op uw eigen computer, zonder hun
          toestemming intekenen op een mailinglijst of een soortgelijke dienst.

<li><em>Illegale inhoud</em><br>

          U mag via e-mail geen gegevens verzenden die men vanuit wettelijk
          oogpunt niet mag bezitten of verzenden.

<li><em>Het schenden van auteursrecht of intellectueel eigendom</em><br>

          U mag auteursrechtelijk beschermd materiaal of intellectuele eigendom
          niet verzenden (via e-mail) of posten, tenzij u hiervoor de
          toestemming heeft.

<li><em>Binaire berichten naar niet-binaire groepen verzenden</em><br>

          Behalve bij het posten naar de nieuwsgroepcategorieën alt.binaries ...
          en alt.pictures ... wordt het posten van gecodeerde binaire gegevens
          als erg ongewenst beschouwd. De meeste Usenet-sites en -lezers hebben
          niet de mogelijkheid om artikelen op een selectieve manier over te
          dragen (met behulp van het zogenaamd killfile-mechanisme) en
          dergelijke berichten kunnen ertoe leiden dat een aanzienlijke
          hoeveelheid middelen wordt geblokkeerd en verspild tijdens het
          overdrachtsproces, en als zodanig kan dit worden beschouwd als een
          denial-of-service-aanval op meerdere ontvangers. [Voorbeeld]

<li><em>Excessief gebruik van crossposten</em><br>

          Eenvoudig gesteld doet deze vorm van onaanvaardbaar gedrag zich voor
          wanneer hetzelfde artikel wordt gepost naar een groot aantal
          nieuwsgroepen die geen verband hebben met elkaar.

<li><em>Excessief gebruik van multiposten</em><br>

          Eenvoudig gesteld doet deze vorm van onaanvaardbaar gedrag zich voor
          wanneer een inhoudelijk vergelijkbaar artikel (waarvan misschien
          enkel de onderwerp-kopregel verschilt) wordt gepost naar een groot
          aantal nieuwsgroepen die geen verband hebben met elkaar.
</ul>


</ol>
