<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The ESnet security team discovered a vulnerability in rssh, a restricted
shell that allows users to perform only scp, sftp, cvs, svnserve
(Subversion), rdist and/or rsync operations. Missing validation in the
scp support could result in the bypass of this restriction, allowing the
execution of arbitrary shell commands.</p>

<p>Please note that with the update applied, the "-3" option of scp can no
longer be used.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.3.4-4+deb8u1.</p>

<p>We recommend that you upgrade your rssh packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1650.data"
# $Id: $
