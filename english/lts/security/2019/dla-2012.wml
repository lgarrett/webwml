<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues have been found in libvpx, a VP8 and VP9 video codec.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9232">CVE-2019-9232</a>

  <p>There is a possible out of bounds read due to a missing bounds check.
  This could lead to remote information disclosure with no additional
  execution privileges needed. User interaction is not needed for
  exploitation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9433">CVE-2019-9433</a>

  <p>There is a possible information disclosure due to improper input
  validation. This could lead to remote information disclosure with
  no additional execution privileges needed. User interaction is
  needed for exploitation.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.3.0-3+deb8u2.</p>

<p>We recommend that you upgrade your libvpx packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2012.data"
# $Id: $
