<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Scott Geary of VendHQ discovered that the Apache HTTPD server used the
value of the Proxy header from HTTP requests to initialize the
HTTP_PROXY environment variable for CGI scripts, which in turn was
incorrectly used by certain HTTP client implementations to configure the
proxy for outgoing HTTP requests. A remote attacker could possibly use
this flaw to redirect HTTP requests performed by a CGI script to an
attacker-controlled proxy via a malicious HTTP request.</p>

<p>For Debian 7 <q>Wheezy</q>, this problem has been fixed in version
2.2.22-13+deb7u7.</p>

<p>We recommend that you upgrade your apache2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-553.data"
# $Id: $
