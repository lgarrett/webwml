<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the chromium web browser.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21201">CVE-2021-21201</a>

    <p>Gengming Liu and Jianyu Chen discovered a use-after-free issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21202">CVE-2021-21202</a>

    <p>David Erceg discovered a use-after-free issue in extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21203">CVE-2021-21203</a>

    <p>asnine discovered a use-after-free issue in Blink/Webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21204">CVE-2021-21204</a>

    <p>Tsai-Simek, Jeanette Ulloa, and Emily Voigtlander discovered a
    use-after-free issue in Blink/Webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21205">CVE-2021-21205</a>

    <p>Alison Huffman discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21207">CVE-2021-21207</a>

    <p>koocola and Nan Wang discovered a use-after-free in the indexed database.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21208">CVE-2021-21208</a>

    <p>Ahmed Elsobky discovered a data validation error in the QR code scanner.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21209">CVE-2021-21209</a>

    <p>Tom Van Goethem discovered an implementation error in the Storage API.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21210">CVE-2021-21210</a>

    <p>@bananabr discovered an error in the networking implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21211">CVE-2021-21211</a>

    <p>Akash Labade discovered an error in the navigation implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21212">CVE-2021-21212</a>

    <p>Hugo Hue and Sze Yui Chau discovered an error in the network configuration
    user interface.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21213">CVE-2021-21213</a>

    <p>raven discovered a use-after-free issue in the WebMIDI implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21214">CVE-2021-21214</a>

    <p>A use-after-free issue was discovered in the networking implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21215">CVE-2021-21215</a>

    <p>Abdulrahman Alqabandi discovered an error in the Autofill feature.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21216">CVE-2021-21216</a>

    <p>Abdulrahman Alqabandi discovered an error in the Autofill feature.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21217">CVE-2021-21217</a>

    <p>Zhou Aiting discovered use of uninitialized memory in the pdfium library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21218">CVE-2021-21218</a>

    <p>Zhou Aiting discovered use of uninitialized memory in the pdfium library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21219">CVE-2021-21219</a>

    <p>Zhou Aiting discovered use of uninitialized memory in the pdfium library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21221">CVE-2021-21221</a>

    <p>Guang Gong discovered insufficient validation of untrusted input.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21222">CVE-2021-21222</a>

    <p>Guang Gong discovered a buffer overflow issue in the v8 javascript
    library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21223">CVE-2021-21223</a>

    <p>Guang Gong discovered an integer overflow issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21224">CVE-2021-21224</a>

    <p>Jose Martinez discovered a type error in the v8 javascript library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21225">CVE-2021-21225</a>

    <p>Brendon Tiszka discovered an out-of-bounds memory access issue in the v8
    javascript library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21226">CVE-2021-21226</a>

    <p>Brendon Tiszka discovered a use-after-free issue in the networking
    implementation.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 90.0.4430.85-1~deb10u1.</p>

<p>We recommend that you upgrade your chromium packages.</p>

<p>For the detailed security status of chromium please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4906.data"
# $Id: $
