<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Simon McVittie discovered a bug in the flatpak-portal service that can
allow sandboxed applications to execute arbitrary code on the host system
(a sandbox escape).</p>

<p>The Flatpak portal D-Bus service (flatpak-portal, also known by its
D-Bus service name org.freedesktop.portal.Flatpak) allows apps in a
Flatpak sandbox to launch their own subprocesses in a new sandbox
instance, either with the same security settings as the caller or
with more restrictive security settings. For example, this is used in
Flatpak-packaged web browsers such as Chromium to launch subprocesses
that will process untrusted web content, and give those subprocesses a
more restrictive sandbox than the browser itself.</p>

<p>In vulnerable versions, the Flatpak portal service passes caller-specified
environment variables to non-sandboxed processes on the host system,
and in particular to the flatpak run command that is used to launch the
new sandbox instance. A malicious or compromised Flatpak app could set
environment variables that are trusted by the flatpak run command, and
use them to execute arbitrary code that is not in a sandbox.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 1.2.5-0+deb10u2.</p>

<p>We recommend that you upgrade your flatpak packages.</p>

<p>For the detailed security status of flatpak please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/flatpak">\
https://security-tracker.debian.org/tracker/flatpak</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4830.data"
# $Id: $
