#use wml::debian::translation-check translation="17865d0a77ccd4d457ca7ff98bccbffdfc7807d7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans QEMU, un
émulateur rapide de processeur.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20257">CVE-2021-20257</a>

<p>net : e1000 : boucle infinie lors du traitement des descripteurs de
transmission.</p>
</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20255">CVE-2021-20255</a>

<p>Un débordement de pile à l'aide d'une vulnérabilité de récursion infinie
a été découverte dans l’émulateur de QEMU du périphérique i8255x eepro100.
Ce problème se produit lors du traitement de commandes de contrôleur dû à un
problème de réentrée DMA. Ce défaut permet à un utilisateur client ou à un
processus de consommer des cycles de CPU ou de planter le processus QEMU sur
l’hôte, aboutissant à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20203">CVE-2021-20203</a>

<p>Un problème de dépassement d'entier a été découvert dans l’émulateur NIC
vmxnet3 de QEMU. Cela pouvait se produire si un client devait fournir des
valeurs non valables pour la taille de la file d’attente rx/tx ou d’autres
paramètres NIC. Un utilisateur client privilégié pouvait utiliser ce défaut
pour planter le processus QEMU sur l’hôte, aboutissant à un scénario de déni
de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3416">CVE-2021-3416</a>

<p>Un débordement potentiel de pile à cause d’un problème de boucle infinie
a été découvert dans divers émulateurs NIC de QEMU dans les versions
jusqu’à la version 5.2.0 (comprise). Le problème se produit dans le mode
loopback d’un NIC dans lequel des vérifications de DMA réentrant sont
contournées. Un utilisateur ou un processus client pourrait utiliser ce défaut
pour consommer des cycles de CPU ou pour planter le processus QEMU sur l’hôte
aboutissant à un scénario de déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3416">CVE-2021-3416</a>

<p>Le correctif pour les
<a href="https://security-tracker.debian.org/tracker/CVE-2020-17380">CVE-2020-17380</a>/<a href="https://security-tracker.debian.org/tracker/CVE-2020-25085">CVE-2020-25085</a>
a été découvert comme inefficace, rendant ainsi QEMU vulnérable à des problèmes
d’accès en lecture ou écriture hors limites, précédemment trouvés dans le code
d’émulation du contrôleur SDHCI. Ce défaut permet à un client privilégié
malveillant de planter le processus QEMU sur l’hôte, aboutissant à un déni de
service ou à une exécution potentielle de code.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:2.8+dfsg-6+deb9u14.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qemu.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de qemu, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/qemu">\
https://security-tracker.debian.org/tracker/qemu</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2623.data"
# $Id: $
