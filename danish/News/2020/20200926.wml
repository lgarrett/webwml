#use wml::debian::translation-check translation="cc3aa11466129a6224ab33a305a554cb8d65f63c"
<define-tag pagetitle>Opdateret Debian 10: 10.6 udgivet</define-tag>
<define-tag release_date>2020-09-26</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.6</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debian-projektet er stolt over at kunne annoncere den sjette opdatering af 
dets stabile distribution, Debian <release> (kodenavn <q><codename></q>).
Denne opdatering indeholder primært rettelser af sikkerhedsproblemer i den 
stabile udgave, sammen med nogle få rettelser af alvorlige problemer.  
Sikkerhedsbulletiner er allerede udgivet separat og der vil blive refereret til 
dem, hvor de er tilgængelige.</p>

<p>Bemærk at denne opdatering ikke er en ny udgave af Debian GNU/Linux
<release>, den indeholder blot opdateringer af nogle af de medfølgende pakker.  
Der er ingen grund til at smide gamle <q><codename></q>-medier væk.  Efter en 
installering, kan pakkerne opgradere til de aktuelle versioner ved hjælp af et 
ajourført Debian-filspejl.</p>

<p>Dem der hyppigt opdaterer fra security.debian.org, behøver ikke at opdatere 
ret mange pakker, og de fleste opdateringer fra security.debian.org er indeholdt 
i denne opdatering.</p>

<p>Nye installeringsfilaftryk vil snart være tilgængelige fra de sædvanlige 
steder.</p>

<p>Opdatering af en eksisterende installation til denne revision, kan gøres ved 
at lade pakkehåndteringssystemet pege på et af Debians mange HTTP-filspejle. En 
omfattende liste over filspejle er tilgængelig på:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Forskellige fejlrettelser</h2>

<p>Denne opdatering til den stabile udgave tilføjer nogle få vigtige rettelser 
til følgende pakker:</p>

<p>Bemærk at på grund af opbygningsproblemer, er opdateringerne af pakkerne 
cargo, rustc og rustc-bindgen i øjeblikket ikke tilgængelige i arkitekturen 
<q>armel</q>.  De kan blive tilføjet på et senere tidspunkt, hvis problemerne 
bliver løst.</p>

<table border=0>
<tr><th>Pakke</th>					<th>Årsag</th></tr>
<correction arch-test 					"Retter at genkendelse af s390x nogle gange fejler">
<correction asterisk 					"Retter nedbrud ved forhandling af T.38 med afvist stream [CVE-2019-15297], SIP-forespørgsel kan ændre SIP-peers adresse [CVE-2019-18790], AMI-bruger kunne udføre systemkommandoer [CVE-2019-18610], segmenteringsfejl i pjsip viser historik med IPv6-peers">
<correction bacula 					"Retter for store digest-strenge tillader at ondsindet klient kunne forårsage heapoverløb i director'ens hukommelse [CVE-2020-11061]">
<correction base-files 					"Opdaterer /etc/debian_version til punktopdateringen">
<correction calamares-settings-debian 			"Deaktiverer displaymanager-modul">
<correction cargo 					"Ny opstrømsudgave, til understøttelse af kommende Firefox ESR-versioner">
<correction chocolate-doom 				"Retter manglende validering [CVE-2020-14983]">
<correction chrony 					"Forhindrer symlinkkapløb ved skrivning til PID-filen [CVE-2020-14367]; retter temperaturlæsning">
<correction debian-installer 				"Opdaterer Linux-ABI til 4.19.0-11">
<correction debian-installer-netboot-images 		"Genopbygger mod proposed-updates">
<correction diaspora-installer 				"Anvender valgmuligehden --frozen til at bundle installering for at anvende opstrøms Gemfile.lock; ekskluder ikke Gemfile.lock under opgraderinger; overskriv ikke config/oidc_key.pem under opgraderinger; gør config/schedule.yml skrivbar">
<correction dojo 					"Retter forurening af prototype i deepCopy-metode [CVE-2020-5258] og i jqMix-metode [CVE-2020-5259]">
<correction dovecot 					"Retter sync-regression i dsync-sieve-filter; retter håndtering af getpwent-resultat i userdb-passwd">
<correction facter 					"Ændrer Google GCE Metadata-endpoint fra <q>v1beta1</q> til <q>v1</q>">
<correction gnome-maps 					"Retter et problem med fejljusteret rendering af shape-layer">
<correction gnome-shell 				"LoginDialog: Nulstiller auth-prompt ved VT-skift før fade-in [CVE-2020-17489]">
<correction gnome-weather 				"Forhindrer et nedbrud når de opsatte placeringer er ugyldige">
<correction grunt 					"Anvender safeLoad når der indlæses YAML-filer [CVE-2020-7729]">
<correction gssdp 					"Ny stabil opstrømsudgave">
<correction gupnp 					"Ny stabil opstrømsudgave; forhindrer <q>CallStranger</q>-angrebet [CVE-2020-12695]; kræver GSSDP 1.0.5">
<correction haproxy 					"logrotate.conf: Anvender rsyslog-helper i stedet af SysV-initscript; afviser meddelelser hvor <q>chunked</q> mangler i Transfer-Encoding [CVE-2019-18277]">
<correction icinga2 					"Retter symlinkangreb [CVE-2020-14004]">
<correction incron 					"Retter oprydning blandt zombieprocesser">
<correction inetutils 					"Retter problem med fjernudførelse af kode [CVE-2020-10188]">
<correction libcommons-compress-java 			"Retter problem med lammelsesangreb [CVE-2019-12402]">
<correction libdbi-perl 				"Retter hukommelseskorruption i XS-funktioner når Perl-stakken reallokeres [CVE-2020-14392]; retter et bufferoverløb ved et for langt DBD-klassenavn [CVE-2020-14393]; retter en NULL-profil-dereference i dbi_profile() [CVE-2019-20919]">
<correction libvncserver 				"libvncclient: Springer fra hvis UNIX-socket-navn kunne løbe over [CVE-2019-20839]; retter problem med pointer-aliasing/-justering [CVE-2020-14399]; begrænser maksimal textchat-størrelse [CVE-2020-14405]; libvncserver: Tilføjer manglende NULL-pointer-kontroller [CVE-2020-14397]; retter problem med pointer-aliasing/-alignment [CVE-2020-14400]; scale: Cast til 64 bit før shifting [CVE-2020-14401]; forhindrer OOB-tilgange [CVE-2020-14402 CVE-2020-14403 CVE-2020-14404]">
<correction libx11 					"Retter heltalsoverløb [CVE-2020-14344 CVE-2020-14363]">
<correction lighttpd 					"Tilbagefører brugbarheds- og sikkerhedsrettelser">
<correction linux 					"Ny stabil opstrømsudgave; forøger ABI til 11">
<correction linux-latest 				"Opdaterer til -11 Linux-kerne-ABI">
<correction linux-signed-amd64 				"Ny stabil opstrømsudgave">
<correction linux-signed-arm64 				"Ny stabil opstrømsudgave">
<correction linux-signed-i386 				"Ny stabil opstrømsudgave">
<correction llvm-toolchain-7 				"Ny opstrømsudgave, til understøttelse af kommende Firefox ESR-versioner; retter fejl som påvirker opbygning af rustc">
<correction lucene-solr 				"Retter sikkerhedsproblem i håndtering af DataImportHandler-opsætning [CVE-2019-0193]">
<correction milkytracker 				"Retter heapoverløb [CVE-2019-14464], stakoverløb [CVE-2019-14496], heapoverløb [CVE-2019-14497], anvendelse efter frigivelse [CVE-2020-15569]">
<correction node-bl 					"Retter overlæsningssårbarhed [CVE-2020-8244]">
<correction node-elliptic 				"Forhindrer letbearbejdighed og overløb [CVE-2020-13822]">
<correction node-mysql 					"Tilføjer localInfile-valgmulighed til kontrol af LOAD DATA LOCAL INFILE [CVE-2019-14939]">
<correction node-url-parse 				"Retter utilstrækkelig validering og rensning af brugerinddata [CVE-2020-8124]">
<correction npm 					"Viser ikke adgangskode i logninger [CVE-2020-15095]">
<correction orocos-kdl 					"Fjerner eksplicit medtagelse af standard-include-sti, retter problemer med cmake &lt; 3.16">
<correction postgresql-11 				"Ny stabil opstrømsudgave; opsæt en sikker search_path i lokgiske replikeringswalsenders og påfører workers [CVE-2020-14349]; gør contrib-modulers installeringsscripts mere sikre [CVE-2020-14350]">
<correction postgresql-common 				"Drop ikke plpgsql før test af udvidelser">
<correction pyzmq 					"Asyncio: Venter på senders POLLOUT i can_connect">
<correction qt4-x11 					"Retter bufferoverløb i XBM-fortolker [CVE-2020-17507]">
<correction qtbase-opensource-src 			"Retter bufferoverløb i XBM-fortolker [CVE-2020-17507]; retter defekt klippebord når timer wrapper efter 50 dage">
<correction ros-actionlib 				"Indlæser YAML på sikker vis [CVE-2020-10289]">
<correction rustc 					"Ny opstrømsudgave, til understøttelse af kommende Firefox ESR-versioner">
<correction rust-cbindgen 				"Ny opstrømsudgave, til understøttelse af kommende Firefox ESR-versioner">
<correction ruby-ronn 					"Retter håndtering af UTF-8-indhold på manpages">
<correction s390-tools 					"Hårdkodet perl-afhængighed i stedet for at anvende ${perl:Depends}, retter installering under debootstrap">
</table>


<h2>Sikkerhedsopdateringer</h2>

<p>Denne revision tilføjer følgende sikkerhedsopdateringer til den stabile 
udgave.  Sikkerhedsteamet har allerede udgivet bulletiner for hver af de nævnte
opdateringer:</p>

<table border=0>
<tr><th>Bulletin-id</th>  <th>Pakke(r)</th></tr>
<dsa 2020 4662 openjdk-11>
<dsa 2020 4734 openjdk-11>
<dsa 2020 4736 firefox-esr>
<dsa 2020 4737 xrdp>
<dsa 2020 4738 ark>
<dsa 2020 4739 webkit2gtk>
<dsa 2020 4740 thunderbird>
<dsa 2020 4741 json-c>
<dsa 2020 4742 firejail>
<dsa 2020 4743 ruby-kramdown>
<dsa 2020 4744 roundcube>
<dsa 2020 4745 dovecot>
<dsa 2020 4746 net-snmp>
<dsa 2020 4747 icingaweb2>
<dsa 2020 4748 ghostscript>
<dsa 2020 4749 firefox-esr>
<dsa 2020 4750 nginx>
<dsa 2020 4751 squid>
<dsa 2020 4752 bind9>
<dsa 2020 4753 mupdf>
<dsa 2020 4754 thunderbird>
<dsa 2020 4755 openexr>
<dsa 2020 4756 lilypond>
<dsa 2020 4757 apache2>
<dsa 2020 4758 xorg-server>
<dsa 2020 4759 ark>
<dsa 2020 4760 qemu>
<dsa 2020 4761 zeromq3>
<dsa 2020 4762 lemonldap-ng>
<dsa 2020 4763 teeworlds>
<dsa 2020 4764 inspircd>
<dsa 2020 4765 modsecurity>
</table>


<h2>Debian Installer</h2>

<p>Installeringsprogrammet er opdateret for at medtage rettelser indført i stable, 
i denne punktopdatering.</p>


<h2>URL'er</h2>

<p>Den komplette liste over pakker, som er ændret i forbindelse med denne 
revision:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Den aktuelle stabile distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Foreslåede opdateringer til den stabile distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Oplysninger om den stabile distribution (udgivelsesbemærkninger, fejl, 
osv.):</p>

<div class="center">
  <a href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sikkerhedsannonceringer og -oplysninger:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>


<h2>Om Debian</h2>

<p>Debian-projektet er en organisation af fri software-udviklere som frivilligt
bidrager med tid og kræfter, til at fremstille det helt frie styresystem Debian
GNU/Linux.</p>


<h2>Kontaktoplysninger</h2>

<p>For flere oplysninger, besøg Debians websider på 
<a href="$(HOME)/">https://www.debian.org/</a> eller send e-mail på engelsk til
&lt;press@debian.org&gt; eller kontakt holdet bag den stabile udgave på 
&lt;debian-release@debian.org&gt;.</p>
