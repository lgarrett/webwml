#use wml::debian::translation-check translation="b917e690cbacd447496fcc36bb3b22df5d6873b2" mindelta="1"
<define-tag description>säkerhetsuppdatering</define-tag>
<define-tag moreinfo>
<p>Flera sårbarheter har upptäckts i Apache HTTPD-servern.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9517">CVE-2019-9517</a>

    <p>Jonathan Looney rapporterade att en illasinnad klient kunde utföra
    ett överbelastningsangrepp (förbruka h2-arbetare) genom att
    överbelasta en anslutning med förfrågningar och helt enkelt aldrig
    läsa svar på TCP-anslutningen.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10081">CVE-2019-10081</a>

    <p>Craig Young rapporterade att HTTP/2-PUSHes kunde leda till
    överskrivning av minne i den pushande förfrågarens pool, vilket
    leder till krascher.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10082">CVE-2019-10082</a>

    <p>Craig Young rapporterade att HTTP/2-sessionshanteringen kunde göras
    att läsa minne som frigjorts, under avslutande av anslutning.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10092">CVE-2019-10092</a>

    <p>Matei <q>Mal</q> Badanoiu rapporterade en begränsade sajtöverskridande
    skriptsårbarhet i felsidan för mod_proxy.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10097">CVE-2019-10097</a>

    <p>Daniel McCarney rapporterade att när mod_remoteip var konfigurerad att
    använda en pålitlig intermediär proxyserver som använder protokollet
    <q>PROXY</q>, kunde en speciellt skapad PROXY-rubrik trigga ett
    stackbuffertspill eller NULL-pekardereferens. Denna sårbarhet kunde
    endast triggas genom en betrodd proxy och inte genom icke betrodda
    HTTP-klienter. Problemet påverkar inte utgåvan Stretch.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10098">CVE-2019-10098</a>

    <p>Yukitsugu Sasaki rapporterade en potentiellt öppen omdirigeringssårbarhet
    i modulen mod_rewrite.</p></li>

</ul>

<p>För den gamla stabila utgåvan (Stretch) har dessa problem rättats
i version 2.4.25-3+deb9u8.</p>

<p>För den stabila utgåvan (Buster) har dessa problem rättats i
version 2.4.38-3+deb10u1.</p>

<p>Vi rekommenderar att ni uppgraderar era apache2-paket.</p>

<p>För detaljerad säkerhetsstatus om apache2 vänligen se
dess säkerhetsspårare på
<a href="https://security-tracker.debian.org/tracker/apache2">https://security-tracker.debian.org/tracker/apache2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4509.data"
